TITLE   arrays1         (hw0918ilgerman.asm)

;  
; Last update: 09/18/2014   written by Ihar Laziuk
; ; = // in java
INCLUDE Irvine32.inc

.code   ; define variables
 x byte "German" ,0
 z word 10,30,30,40,50,60,70,80
 y word 10,30,30,40,50,60,70,80
 w dword 10,20,30,40,50,60,70,80
 
main PROC


;for german.asm
; 1. Print out the 10, 20, 30, ... for w
; 2. Explain w+1, W+2, w+3
; 3. Explain Y+17
; 4. Explain the numbers in class
; 5. new homework on yahoo groups for Tuesday

        mov eax,0
       mov al,  [ x+5]    
       call writechar
     mov al,  [x+4]    
       call writechar
 mov al,  [ x+3]    
       call writechar
     mov al,  [x+2]    
       call writechar
 mov al,  [ x+1]    
       call writechar
     mov al,  x    
       call writechar
    call crlf
   mov edx, offset x
  call writestring
 call crlf
   mov eax,0
       mov ax,  [ y]    
       call writedec
 call crlf
     mov ax,  [y+1]    
       call writedec
 call crlf
   mov ax,  [y+2]    
       call writedec
 call crlf
     mov ax,  [y+3]    
       call writedec
 call crlf
 mov ax,  [ y+4]    
       call writedec
 call crlf
     mov ax,  [y+5]   
       call writedec
    call crlf
	call crlf
	call crlf

	mov eax,0
	mov eax, w
	call writedec
	call crlf


	mov eax, w+4
	call writedec
	call crlf
	
	
	mov eax, w+8
	call writedec
	call crlf

	mov eax, w+12
	call writedec
	call crlf

	mov eax, w+16
	call writedec
	call crlf
	
	mov eax, w+20
	call writedec
	call crlf

	mov eax, w+24
	call writedec
	call crlf

	mov eax, w+28
	call writedec
	call crlf
	call crlf

	;mov ax,0
	mov ax,y+17
	call writedec
	call crlf
	
	exit
main ENDP
END main