TITLE Alphabet (test2ILalphabet.asm)

COMMENT @
Puprose: print my initials
Programmer: Ihar Laziuk
Last updated: 11.06.2014
@

INCLUDE Irvine32.inc

.data
	blank byte ' ',0
	begin dword '65'
	end_ dword '90'


.code


main PROC

mov ecx, end_
sub ecx, begin
inc ecx
mov eax, ecx

	ABC:
		
		Mov eax, begin

		Call writechar	;writeint to print negative numbers
		mov edx, offset blank
		call writestring 
		inc begin

	loop ABC

	call crlf 

	exit
main ENDP
END main

