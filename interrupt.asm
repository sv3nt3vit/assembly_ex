	TITLE Project_2

	; Author:	Ihar Laziuk
	; Date:		02.15.2015

	;INCLUDE IRVINE32.INC

	.DATA

					MSG1 		BYTE 'ENTER A STRING ', 0
					MSG2 		BYTE 'ENTEREd string is : ', 0
					ENTRY 		BYTE 30 DUP(0)	

	.CODE
	
	EXIT			MACRO
					MOV 	AX, 4C00h
					INT		21h
					ENDM
					
	MAIN 			PROC
	
					ORG			100h
					JMP			START
					
	START:			MOV			DX, OFFSERT MSG1
					MOV			AH, 09
					INT			21H
	
					CALL 		CRLF
					
					
						
					MOV 		EDX, OFFSET MSG2
					ORG			100h
					JMP			START
					
					MOV 		EDX, OFFSET ENTRY
					CALL 		WRITESTRING
		
					EXIT
	MAIN 			ENDP
	
					END 		MAIN
