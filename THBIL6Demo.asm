TITLE Euclid's algorithm (THBIL6.asm)

COMMENT @
Puprose: demonstrates Euclid's algorithm
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@

INCLUDE Irvine32.inc

.data

userMsg1 byte 'Enter the 1st number : ', 0
userMsg2 byte 'Enter the 2nd number : ',0
GCDmsg byte 'The greatest common divisor is ',0

remainder dword ?
aa dword ?	;var to hold initial A value
bb dword ?	;var to hold initial B value
cc dword ?
count dword 0
result dword ?

.code
main PROC

    call clrscr
	call crlf
	mov edx, offset userMsg1
     call writestring
     call readint
	 mov aa, eax	;just to print later
     mov result, eax
     call crlf
     mov edx, offset userMsg2
     call writestring
     call readint
	 mov bb, eax	;just to print later
     call crlf
	
	call Euclid
	
	call crlf

exit
main ENDP


Euclid PROC

	mov edx,0
	mov ecx,0
	
.while (eax!=0)
	;mov cc, aa mod bb
	;mov aa,eax
	;mov eax, cc
	xor edx,edx
	push eax
	mov eax, aa
	mov ecx, bb
	div ecx
	mov cc, edx
	pop eax
	mov aa, eax
	mov eax, cc
.endw

	mov edx, offset GCDmsg
	call writestring
	mov eax, aa
	call writedec


ret
Euclid ENDP

end MAIN