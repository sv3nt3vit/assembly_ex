TITLE: Boolean Calculator with menu (THILbooleanCalc.asm)

COMMENT @
Puprose: boolean calculator
		performs operations
		AND, XOR, OR, NOT .
Programmer: Ihar Laziuk
Last updated: 12.10.2014
@
INCLUDE Irvine32.inc

.data

str1 byte "Enter an hex integer: ",0
str2 byte "Enter 2nd hex integer: ",0
result byte "Result: ",0
caseTable byte '1'      ;lookup value
dword Process_1 		;address of lookup value
entrySize = ($ - caseTable )
          byte '2'
          dword Process_2
          byte '3'
          dword Process_3
          byte '4'
          dword Process_4
          byte '5'
          dword Process_5
numberOfEntries = 5

menuText  byte "-------------------------------------",0dh,0ah	;for location purpose
          byte "   32-Bit Boolean Calculator by IL"     ,0dh,0ah
          byte "-------------------------------------",0dh,0ah
	  byte "1. x AND y"     ,0dh,0ah
	  byte "2. x OR y"      ,0dh,0ah
	  byte "3. NOT x"       ,0dh,0ah
	  byte "4. x XOR y"     ,0dh,0ah
	  byte "5. Exit Program",0dh,0ah
	  byte "-------------------------------------",0dh,0ah
          byte "Enter desired operation 1 - 5: ",0
msg1      byte "Option 1 (x AND y)",0dh,0ah,0
msg2      byte "Option 2 (x OR y)",0dh,0ah,0
msg3      byte "Option 3 (NOT x)",0dh,0ah,0
msg4      byte "Option 4 (x XOR y)",0dh,0ah,0
msg5      byte "Option 5 (Exiting Program...)",0dh,0ah,0

.code
main PROC

        call Clrscr              
        call Menu               

EVACUATE::exit                   ;label to exit the program
main ENDP

;================================ MENU ====================================

Menu PROC

	mov  edx,OFFSET menuText 
	call WriteString         	;display menuText
	call ReadChar		 	 	;read user input
    mov  ebx,OFFSET caseTable	;point EBX to the table
	mov  ecx,numberOfEntries 	;loop counter

L1:
	cmp  al,[ebx]		 ;find appropriate menu operation
	jne  L2		         ;if not found no: continue
	call NEAR PTR [ebx + 1]	 ;yes: call the procedure
	;call WriteString	 ;display message
	call Crlf                ;
	call WaitMsg             ;pause message
	call Clrscr             
	jmp  L3		         ;exit the search

   L2:
	add  ebx,5		 ;point to the next entry
loop L1		         ;repeat until ECX = 0

   L3:
	jmp Menu	         ;run Menu again

Menu EndP

;================================ END MENU =================================




;################################# AND ####################################
Process_1 PROC

	mov  edx,OFFSET msg1     
	call Crlf               
	call Crlf
	
	call WriteString	 ;display operation called
	call Crlf  

	mov edx,OFFSET str1     ;Enter first hex prompt
		call WriteString    ;Display message
		call ReadHex        ;Read the input
	mov ebx,eax				;move hex content to EBX
	mov edx,OFFSET str2		;Enter second hex prompt
		call WriteString	;Display message
		call ReadHex		;Read the input
	AND eax,ebx				;AND cotents of EAX and EBX

	mov edx,OFFSET result   ;Results out
		call WriteString	;Display message
		call WriteHex       ;Display hex results
		call Crlf			
		;call WaitMsg		;Wait for user to continue
	
	ret                      
Process_1 ENDP
;################################# END AND #################################




;================================ OR ====================================
Process_2 PROC

	mov  edx,OFFSET msg2     
	call Crlf               
	call Crlf   

	call WriteString	 ;display operation called
	call Crlf  
	
	mov edx,OFFSET str1     ;Enter first hex prompt
		call WriteString    ;Display message
		call ReadHex        ;Read the input
	mov ebx,eax				;move hex content to EBX
	mov edx,OFFSET str2		;Enter second hex prompt
		call WriteString	;Display message
		call ReadHex		;Read the input
	OR eax,ebx				;OR cotents of EAX and EBX

	mov edx,OFFSET result   ;Results out
		call WriteString	;Display message
		call WriteHex       ;Display hex results
		call Crlf		
	
	ret                     
Process_2 ENDP
;================================ END OR =================================



;################################# NOT ####################################
Process_3 PROC

	mov  edx,OFFSET msg3     
	call Crlf              
	call Crlf 
	
	call WriteString	 ;display operation called
	call Crlf  

	mov edx,OFFSET str1     ;Enter first hex prompt
		call WriteString    ;Display message
		call ReadHex        ;Read the input

	NOT eax					;reverse bits

	mov edx,OFFSET result   ;Results out
		call WriteString	;Display message
		call WriteHex       ;Display hex results
		call Crlf		
	
	ret                   
Process_3 ENDP
;################################# END NOT #################################



;================================ XOR ====================================
Process_4 PROC

	mov  edx,OFFSET msg4     
	call Crlf            
	call Crlf   
	
	call WriteString	 ;display operation called
	call Crlf  

	mov edx,OFFSET str1     ;Enter first hex prompt
		call WriteString    
		call ReadHex        ;Read the input
	mov ebx,eax				;move hex content to EBX
	mov edx,OFFSET str2		;Enter second hex prompt
		call WriteString	
		call ReadHex		;Read the input
		
	XOR eax,ebx				;XOR cotents of EAX and EBX

	mov edx,OFFSET result   ;Results out
		call WriteString	;Display message
		call WriteHex       ;Display hex results
		call Crlf			
	
	ret                   
Process_4 ENDP
;================================ END XOR =================================



;################################# EXIT ####################################
Process_5 PROC

	mov  edx,OFFSET msg5     
	call Crlf               
	call Crlf               
	call WriteString	 ;display message
	call Crlf               
	stc	                 ; set carry flag, CF = 1
	jc EVACUATE    	     ;if CF=1 jump to EVACUATE

Process_5 ENDP
;################################# END EXIT #################################

END main