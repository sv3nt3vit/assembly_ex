	TITLE Project_2

	; Author:	Ihar Laziuk
	; Date:		02.15.2015
	; 1. ask user to enter a string 
	; 2. put it into buffer, 
	; 3. create iterating construct
	; 4. use cmp, jmp, ptr arithmetic
	; 5. count characters and display

	INCLUDE IRVINE32.INC

	.DATA
						ENTRY 		BYTE 30 DUP(0)	
						MSG1 		BYTE 'ENTER A STRING ', 0
						MSG2 		BYTE 'ENTERED STRING HAS LENGTH OF CHRACTERS: ', 0
	.CODE
	
	ITERATE				PROC
						
						PUSH		EBP
						MOV 		EBP, ESP
						PUSH		ECX
						PUSH		EBX
						MOV			ECX, [EBP+8]
	
	ONE_LOOP:			MOV     	BL, [ECX + EAX]
						CMP	    	BL, 0
						JE	    	TWO_LOOP
						ADD     	EAX, 1
						JMP     	ONE_LOOP 	
	
	TWO_LOOP:			POP	    	EBX
						POP     	ECX
						POP     	EBP
						RET     	4
												
	ITERATE				ENDP
	
	GET_ENTRY			PROC
	
						PUSH		EBP
						MOV			EBP, ESP
						PUSH		EDX
						MOV			EDX, [EBP+8]
						CALL		WRITESTRING
						POP 		EDX
						POP			EBP
						RET			4
	GET_ENTRY			ENDP
						
	
	MAIN 				PROC
	
						PUSH		OFFSET MSG1
						CALL		GET_ENTRY
						MOV 		EDX, OFFSET ENTRY							 
						MOV 		ECX, SIZEOF ENTRY			
						CALL		READSTRING
						CALL		CRLF
						
						PUSH		OFFSET ENTRY
						CALL		GET_ENTRY
						PUSH		OFFSET ENTRY
						CALL		ITERATE
						CALL		CRLF
						MOV 		EDX, OFFSET MSG2
						CALL 		WRITESTRING
						CALL		WRITEDEC
							 		
						EXIT
	MAIN 				ENDP
	
						END 		MAIN
