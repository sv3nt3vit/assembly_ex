TITLE Integers Counting  Stats	(integerCount.asm)

;Written by Ihar Laziuk
;Last Update: 10/23/2014

INCLUDE Irvine32.inc

.data

x byte 1,1,2,3,3,2,4,2,4,2,1,2,3,2,1
space byte ' ',0
count byte 5 DUP(0)

.code

main PROC
	
	mov eax, 0
	mov esi, 0	
	mov edx, offset space	
	mov ecx, lengthof x	

	cntloop:
		mov al, x[esi]
		movzx edi, al
		inc count[edi]
		inc esi		
	loop cntloop;

	mov esi, 0
	mov eax, 0
	mov ecx, lengthof count

	displayLoop:
		mov al, count[esi]
		call writedec 
		call writestring
		inc esi
	loop displayLoop;

exit
main ENDP
END main