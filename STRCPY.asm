	TITLE STRCPY

	; AUTHOR:	IHAR LAZIUK
	; DATE:		04.19.2015
	; CREATE A PROC THAT
	; COPIES A STRING


	INCLUDE IRVINE32.INC

	.DATA
						BUFFER 		BYTE 	30 DUP(0)
						COPIED 	    BYTE 	30 DUP(0)				
						MSG1 			BYTE 	'ENTER A STRING:  ', 0
						MSG2 			BYTE 	'COPIED STRING:  ', 0							
	.CODE


	STRLEN			PROC						
						PUSH			EBP	
						MOV				EBP, ESP	
						PUSH			ECX				
						PUSH			EBX	
						MOV				ECX, [EBP + 8]

	ONE_LOOP:	
						MOV				BH, [ECX + EAX]
						CMP				BH, 0
						JE				EVACUATE
						INC				EAX
						JMP				ONE_LOOP
	EVACUATE:				
						POP				EBX	
						POP				ECX	
						POP				EBP	
						RET				4			
	STRLEN			ENDP
	

	STRCPY 	    PROC
						PUSH      	  	EBP
						MOV				EBP, ESP	
						PUSH			ESI
						PUSH			EDI
						PUSH			EAX
						PUSH       	EBX
						
						MOV 			ESI, [EBP+12]
						MOV				EDI, [EBP+8]				
						PUSH			[EBP + 12 ]
						CALL      	 	STRLEN
						
	ONE_LOOP:
						MOV				BL, [ESI]
						CMP				BL, 0
						JE				EVACUATE
						MOV        		[EDI], BL
						INC				ESI
						INC				EDI
						JMP				ONE_LOOP	
	EVACUATE:	    
						POP				EBX
						POP				EAX
						POP				EDI
						POP				ESI
						POP 				EBP
						RET				4
	STRCPY	    ENDP

	
	MAIN   			PROC
	
						MOV 			EDX, OFFSET MSG1
						CALL 			WRITESTRING
						
						MOV	    		ECX, SIZEOF BUFFER 
						MOV	   			EDX, OFFSET BUFFER
						CALL    		READSTRING
				  
						MOV 			EDX, OFFSET MSG2
						CALL 			WRITESTRING
						
						PUSH    		OFFSET BUFFER
						PUSH    		OFFSET COPIED
						CALL    		STRCPY

						MOV 			EDX, OFFSET COPIED
						CALL 			WRITESTRING
						
						EXIT
	MAIN 	   		ENDP

						END    			MAIN
