TITLE   hw3ILaddsword         (hw3ILaddsword.asm)
; Last update: 09/29/2014   written by Ihar Laziuk

INCLUDE Irvine32.inc

.code
	val1 sword 16
	val2 sword 5
	val3 sword 18
	
main  proc
	
	movzx eax,val1
	neg eax
	add ax,val2
	sub ax,val3
	
	call DumpRegs
	call writedec
	call crlf
	call writeint
	
	exit
main  endp
end   main
