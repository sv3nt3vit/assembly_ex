TITLE allTable                            (hw1014ilallTable.asm)

;This Program prints out multiplication table for numbers 1-10
;Last updated 10/16/2014   Written by Ihar Laziuk
INCLUDE Irvine32.inc

.data
	i byte 0
	timez byte "*"
	equals byte "="
	sum dword 0
	number dword 1
	temp dword ?
	 
.code
main PROC		
		call clrscr
		call crlf
		mov eax,0
		mov ecx,10
        outerLoop:
		 call alltables	;externalize the code and call a procedure
         loop outerLoop
        
        exit	 
main ENDP
alltables proc
      ;instead of this use next line (more efficient) mov temp, ecx
	push ecx
      mov ecx, 11
		  innerLoop:
		     mov eax, number
		     call writedec
		     mov eax,0
		     mov al, timez
		     call writechar
		     mov al,i
		     call WriteDec
		      mov eax,0
	             mov al, equals
		     call WriteChar
		     mov eax,0
		     mov eax, sum
		     call WriteDec
		     call crlf
		     mov eax, number
		     add sum, eax
		     inc i
		   loop innerLoop
		mov i,0		
		inc number
	; instead of thi suse next line command mov ecx, temp
		pop ecx
		mov sum,0
		call crlf
                ret
alltables endp
end main