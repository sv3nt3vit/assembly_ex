TITLE Multiply   special edition                            (hw1014ilmult.asm)

COMMENT @ 
This Program performs multiplication using 3 different methods:
1. through additon, adds y x times
2. using mul menmonic
3. calling a procedure
Last updated 10/16/2014   Written by Ihar Laziuk
@
INCLUDE Irvine32.inc

.data
	y dword 2
	x dword 6
	 
.code
main PROC		
		call clrscr
		call crlf	
;method 1 - multiply by addition		
		mov eax, 0
		mov ecx, x
		
		L1:
			add eax, y
		loop L1
		
		call writedec
		
		call crlf
		
;method 2 - using mul mnemonic ec:)
		
		mov eax, 0
		mov eax, y
		mul x
		call writedec

		call crlf
		
;method 3 - mult by calling a procedure ec:)

		call multiply
		
		COMMENT @
		info from CSC230, how to multiply by addition pseudocode 
		i = 0
		j=0
		while (i!=x){
			j=j+y;
			i=i+1;
		}
        @
		
        exit	 
main ENDP

multiply PROC
	mov eax, 0
	mov ecx, x
		
	L1:
		add eax, y
	loop L1
		
	call writedec	
	call crlf
ret
multiply ENDP

end main