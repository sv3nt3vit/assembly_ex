TITLE printing initials  upside down        (initialsUpDown.asm)
  
COMMENT @
Puprose: print my initials upside down
Programmer: Ihar Laziuk
Last updated: 11.06.2014
@

INCLUDE Irvine32.inc

.data  

 array0 byte 1,"                          "; blank before to prevent trail when moving
 array1 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
 array2 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
 array3 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
 array4 byte 1,' ', 2,'x',3,' ',5,'x',1,' ' 
 array5 byte 1, "                         " ;blank after to prevent trail when shifting
 delta dword ?
 lenarray1 = lengthof array1
 lenarray2 = lengthof array2
 lenarray3 = lengthof array3
 lenarray4 = lengthof array4
 xa byte 5
 ya byte 10

.code


main proc

 call clrscr
 mov eax, 400
 call delay
 mov ecx,30
 push ecx
 
movement:
 	call figure 
 	inc xa  
 	pop ecx
loop movement

exit

;****************additional procedures*************

drawline proc
    mov ecx, delta
    lineloop:    
       call writechar
       inc dl
    loop lineloop
    ret
drawline endp


figure proc

 mov dh,ya
 mov dl,xa   
 call gotoxy   
 mov esi,0     
 mov ecx,lenarray4/2

 outer1:
    movzx eax,array4[esi]  
    push ecx
    mov  delta, eax
    inc esi 
    movzx eax,array4[esi]
    call drawline
    inc esi
    pop ecx 
 loop outer1

 inc dh
 mov dl,xa   
 call gotoxy    
 mov esi,0   
 mov ecx,lenarray2/2


 outer2:
    movzx eax,array2[esi]  
    push ecx
    mov  delta, eax
    inc esi 
    movzx eax,array2[esi]
    call drawline
    inc esi
    pop ecx 
 loop outer2


 inc dh
 mov dl,xa 
 call gotoxy    
 mov esi,0    
 mov ecx,lenarray3/2

 outer3:
    movzx eax,array3[esi]   
    push ecx
    mov  delta, eax
    inc esi 
    movzx eax,array3[esi]
    call drawline
    inc esi
    pop ecx  
loop outer3

 inc dh   
 mov dl,xa  
 call gotoxy   
 mov esi,0     
 mov ecx,lenarray1/2

 outer4:
    movzx eax,array1[esi]   
    push ecx
    mov  delta, eax
    inc esi 
    movzx eax,array1[esi]
    call drawline
    inc esi
    pop ecx 
 loop outer4

 inc dh
 mov dl,xa   
 call gotoxy    
 mov esi,0     
 mov ecx,lenarray1/2

outer5:
    movzx eax,array1[esi]   
    mov  delta, eax
    inc esi 
    movzx eax,array1[esi]
    call drawline
    inc esi
    pop ecx  
loop outer5 

ret

figure endp

main ENDP
END main