	TITLE STRCAT

	; AUTHOR:	IHAR LAZIUK
	; DATE:		05.09.2015
	; CREATE A PROC THAT
	; PROMPTS USER TO
	; ENTER TWO STRINGS
	; AND PRINTS OUT A NEW
	; CONCATENTAED STRING

	INCLUDE IRVINE32.INC
	
	HANDLE			TEXTEQU		<DWORD>
	
	GetProcessHeap	PROTO
	HeapAlloc		PROTO,		HHEAP: HANDLE, DWFLAGS:DWORD, DWBYTES:DWORD
	HeapFree		PROTO, 		HHEAP: HANDLE, DWFLAGS:DWORD, LPMEM:DWORD	
	

	FREE		MACRO			HANDLE	
				PUSH			HANDLE
				CALL			MEMFREE
				ENDM
	
	
	MALLOC		MACRO			ALLOCSIZE	
				PUSH			ALLOCSIZE
				CALL			MEMALLOC
				ENDM

	
	.DATA
				BUFFER 			BYTE 	15 DUP(0)	
				BUFFER2			BYTE 	15 DUP(0)	
				MSG1 			BYTE 	'ENTER STRING 1: ', 0 							
				MSG2 			BYTE 	'ENTER STRING 2: ', 0
				MSG3 			BYTE 	'CONCATENATED STRING: ', 0														
				;RESULT 			DWORD	30 DUP('0')
	
	.CODE
	
	
	MEMALLOC	PROC				
				PUSH 			EBP
				MOV				EBP, ESP
				INVOKE 			GETPROCESSHEAP
				INVOKE 			HEAPALLOC, EAX, 8, [EBP+8]
				POP 			EBP
				RET				4				
	MEMALLOC	ENDP
	
				
	MEMFREE		PROC			
				PUSH			EBP
				MOV				EBP, ESP
				INVOKE      	GETPROCESSHEAP
				INVOKE			HEAPFREE, EAX, 0, [EBP+8]
				POP				EBP
				RET				4				
	MEMFREE		ENDP
		
	
	STRCPY 		PROC
				PUSH       		EBP
				MOV				EBP, ESP	
				PUSH			ESI
				PUSH			EDI
				PUSH       		EBX				
				MOV 			ESI, [EBP+12]			; THE ADDR OF HEAP
				MOV				EDI, [EBP+8]			; THE STRING				
	ONE_LOOP:
				MOV				BL, [EDI]
				CMP				BL, 0
				JE				EVACUATE
				MOV         	[ESI], BL
				INC				EDI
				INC				ESI
				JMP				ONE_LOOP	
	EVACUATE:	 
				MOV				EAX, [EBP+12]
				POP				EBX
				POP				EDI
				POP				ESI
				POP 			EBP				
				RET				8
	STRCPY	    ENDP

	

	STRDCPY		PROC
				PUSH			EBP
				MOV				EBP, ESP
				PUSH			[EBP+8]
				CALL			STRLEN
				INC				EAX
				MALLOC			EAX			
				PUSH			EAX				; THE ADDR OF HEAP
				PUSH			[EBP+8]			; THE STRING
				CALL			STRCPY
				POP				EBP
				RET				8
	STRDCPY		ENDP

		
	STRLEN		PROC						
				PUSH			EBP	
				MOV				EBP, ESP	
				PUSH			ECX				
				PUSH			EBX	
				MOV				ECX, [EBP + 8]

	ONE_LOOP:	MOV				BH, [ECX + EAX]
				CMP				BH, 0
				JE				EVACUATE
				INC				EAX
				JMP				ONE_LOOP
	EVACUATE:				
				POP				EBX	
				POP				ECX	
				POP				EBP	
				RET				4			
	STRLEN		ENDP
		
	
	
		
	STRCAT		PROC
				PUSH			EBP
				MOV				EBP, ESP
				SUB				ESP, 8

				PUSH			EBX
				PUSH			ECX
				PUSH			EDI
				PUSH			ESI
				
				MOV				ESI,  [EBP+12]
				PUSH			ESI
				CALL			STRLEN				
				MOV				[EBP-8], EAX
				
				MOV				EDI,  [EBP+8]
				PUSH			EDI
				CALL			STRLEN
				INC				EAX				; FOR THE NULL
				MOV				ECX, EAX
				ADD				ECX, [EBP-8]
				
				MALLOC			ECX
				MOV				EBX, EAX		;PASS ALLOC MEMORY /EBX INSTEAD OF ESI
				
				MOV				[EBP-4], EAX	
				PUSH			[EBP+12]
				PUSH			EBX				;CHANGE TO EBX INSTEAD OF ESI
				CALL			STRCPY
				
				MOV				EBX, [EBP-4]	; THE ORIGINAL ADDR OF MEM ALLOCATED / EBX
				ADD				EBX, [EBP-8]	; SIZE OF THE STR 1 / ESI
				
				PUSH			[EBP+8]
				PUSH			EBX				; CHANGE TO EBX
				CALL			STRCPY
				
				MOV				EAX, [EBP-4]	
										
				POP				ESI
				POP				EDI
				POP				ECX
				POP				EBX	
				ADD				ESP, 8	
				
				POP				EBP
			
				RET				8
	STRCAT		ENDP
	
	
	
	MAIN   		PROC
				
				MOV 			EDX, OFFSET MSG1
				CALL 			WRITESTRING

				MOV	    		ECX, SIZEOF BUFFER 
				MOV	   			EDX, OFFSET BUFFER
				CALL    		READSTRING						
				
				MOV				EDX, OFFSET MSG2
				CALL 			WRITESTRING
				
				MOV	    		ECX, SIZEOF BUFFER2 
				MOV	   			EDX, OFFSET BUFFER2
				CALL    		READSTRING
				
				PUSH			OFFSET BUFFER
				PUSH			OFFSET BUFFER2
				CALL			STRCAT
				
				CALL			DUMPREGS
				MOV				EDX, EAX
				CALL 			WRITESTRING
				CALL			DUMPREGS
				
				FREE			EAX	
				
				CALL			WRITEDEC
			
				EXIT
	MAIN 	    ENDP

				END    			MAIN
