TITLE box with stars	 (hw1030ILstarBox.asm)

COMMENT @
Puprose: draw a box and fill it w /stars
Programmer: Ihar Laziuk
Last updated: 10.30.2014
@

INCLUDE Irvine32.inc
	
.data
	symbol byte '*'
    xa byte ?      ;left x
    xb byte ? 
	ya byte ?      ;top y
    yb byte ?      ;bottom y
 
    deltay  byte ?   ;  yb-ya + 1
    deltax  byte ?
.code
main PROC	
	
	call clrscr
	;*********** left to right n
        mov xa,1
        mov xb,20
        call makedx
        mov ya,10
    ;*********** left to right
         
        mov ya,20
        call drawlre
        
    ;*********** up         
       mov ya,1
       mov yb,20
       call makedy
       mov xb,20
       call drawupe
	   
	;*********** right to left   
	mov xb,20
	call makedx
    mov ya,1
	mov yb,20
    call drawrle

	;*********** down
	mov ya, 1
	mov yb, 20
	call makedy
	mov xb, 1
	call drawdown	


	
exit	 
main ENDP

 ; additional procedures   

drawlre proc
      movzx ecx,deltax
      mov dl,xa
      mov dh,ya
      call gotoxy
     
      lre:
         mov al, symbol
         call writechar
         call gotoxy
         mov eax ,50
         call delay
         inc dl
         call gotoxy
      loop lre
      ret
  drawlre endp  

drawrle proc
      movzx ecx,deltax
      mov dl,xb
      mov dh,ya
      call gotoxy
     
      rle:
          mov al, symbol
         call writechar
         call gotoxy
         mov eax ,50 
         call delay
         dec dl
         call gotoxy
      loop rle
      ret
  drawrle endp 

drawupe proc
      movzx ecx,deltay
      mov dl,xb
      mov dh,yb
      call gotoxy
     
      upe:
          mov al, symbol
         call writechar
         call gotoxy
         mov eax ,50 
         call delay
         dec dh
         call gotoxy
      loop upe
      ret
  drawupe endp 


drawdown proc
	movzx ecx, deltay
	mov dl,xb
	mov dh,ya
	call gotoxy
	
	down:
	 mov al, symbol
         call writechar
         call gotoxy
         mov eax ,50 
         call delay
         inc dh
         call gotoxy
      loop down	
	ret
drawdown endp

makedx proc
     mov   al,xb
     sub   al,xa
     inc   al
     mov   deltax, al
     ret
makedx endp  
makedy proc
     mov   al,yb
     sub   al,ya
     inc   al
     mov   deltay, al
     ret
makedy endp  
END MAIN    