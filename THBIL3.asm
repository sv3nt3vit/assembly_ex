TITLE Random nums and arrays	(THBIL3.asm)

COMMENT @
Puprose: generate 100 random
		 numbers 100-200 and
		 put them in an array,
		 copy the array, delete
		 duplicates and print it out.
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@;Last Update: 

INCLUDE Irvine32.inc

.data

array1 dword ?
target dword 100 dup(?)

space byte " ",0

.code

main PROC

call clrscr
call randomize

mov edx, offset space
mov esi, offset array1
mov ecx, 100

l1:
	mov eax, 200
	call randomrange
	sub eax, 100

	mov [esi], eax
	call writeint
	call writestring

	push [esi]

loop l1;

call crlf
call crlf

mov edi, offset target
mov edx, offset space
mov ecx, 100

l2:

pop [esi]
.if(ecx<100)
call writeint
call writestring

.endif

mov eax,[esi]
mov [edi], eax

add edi, 4

loop l2;

pop[esi]
call writeint

call crlf
call crlf

mov edx, offset space

mov ecx, 100

l3:

sub edi, 4

mov eax,[edi]
call writeint
call writestring


loop l3;

exit

main ENDP
END main
 