TITLE draws a triangle 7 lines	 (hw1030ILtriangle.asm)

COMMENT @
Puprose: draw a 7 lines triangle
Programmer: Ihar Laziuk
Last updated: 11.04.2014
@

INCLUDE Irvine32.inc

.data

symbol byte '#',0
temp dword 1
textcolor dword ?

.code

main PROC

	call clrscr
	call randomize

	mov eax, 0
	mov ecx, 7	
	
	myLoop:
		push ecx
		mov ecx, temp
		call triangle
		call crlf
		pop ecx
		inc temp		
	loop myLoop
	
	mov textcolor, 15
    mov eax,(textcolor+(black*15))
    call settextcolor	

exit
main ENDP

triangle PROC

	loopProc:
		mov edx, offset symbol
		call writestring
		mov eax, 8
		call randomrange
		add eax, 8
		mov textcolor, eax
		mov eax, (textcolor + (black*16))
		call settextcolor	
	loop loopProc
	
	ret

triangle ENDP

END main