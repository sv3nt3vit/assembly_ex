	TITLE STRDCPY

	; AUTHOR: IHAR LAZIUK
	; DATE:	05.09.2015
	; CREATE A PROC THAT
	; USES MEMORY HEAP
	; TO PERFORM DEEP COPY
	; OF A STRING

	INCLUDE IRVINE32.INC
	
	HANDLE		TEXTEQU	<DWORD>
	
	GetProcessHeap	PROTO
	HeapAlloc		PROTO,	HHEAP: HANDLE, DWFLAGS:DWORD, DWBYTES:DWORD
	HeapFree		PROTO, 	HHEAP: HANDLE, DWFLAGS:DWORD, LPMEM:DWORD	
	

	FREE			MACRO			HANDLE	
				PUSH			HANDLE
				CALL			MEMFREE
				ENDM
	
	
	MALLOC			MACRO			ALLOCSIZE	
				PUSH			ALLOCSIZE
				CALL			MEMALLOC
				ENDM

	
	.DATA
				BUFFER 			BYTE 	30 DUP(0)
				MSG1 			BYTE 	'ENTER A STRING ', 0
				MSG2 			BYTE 	'COPIED STRING: ', 0
	
	.CODE
	
	
	MEMALLOC		PROC				
				PUSH 			EBP
				MOV			EBP, ESP
				INC			EBX
				INVOKE 			GETPROCESSHEAP
				INVOKE 			HEAPALLOC, EAX, 8, [EBP+8]
				POP 			EBP
				RET			4				
	MEMALLOC		ENDP
	
				
	MEMFREE			PROC			
				PUSH			EBP
				MOV			EBP, ESP
				INVOKE      		GETPROCESSHEAP
				INVOKE			HEAPFREE, EAX, 0, [EBP+8]
				POP			EBP
				RET			4				
	MEMFREE			ENDP
	

	STRCPY 			PROC
				PUSH       		EBP
				MOV			EBP, ESP	
				PUSH			ESI
				PUSH			EDI
				PUSH       		EBX				
				MOV 			ESI, [EBP+12]			; THE ADDR OF HEAP
				MOV			EDI, [EBP+8]			; THE STRING				
	ONE_LOOP:
				MOV			BL, [EDI]
				CMP			BL, 0
				JE			EVACUATE
				MOV         		[ESI], BL
				INC			EDI
				INC			ESI
				JMP			ONE_LOOP	
	EVACUATE:	    
				POP			EBX
				POP			EDI
				POP			ESI
				POP 			EBP				
				RET			8
	STRCPY	    		ENDP

	

	STRDCPY			PROC
				PUSH			EBP
				MOV			EBP, ESP
				PUSH			[EBP+8]
				CALL			STRLEN
				INC			EAX
				MALLOC			EAX			
				PUSH			EAX				; THE ADDR OF HEAP
				PUSH			[EBP+8]				; THE STRING
				CALL			STRCPY
				POP			EBP
				RET			4
	STRDCPY			ENDP

		
	STRLEN			PROC						
				PUSH			EBP	
				MOV			EBP, ESP	
				PUSH			ECX				
				PUSH			EBX	
				MOV			ECX, [EBP + 8]

	ONE_LOOP:		MOV			BH, [ECX + EAX]
				CMP			BH, 0
				JE			EVACUATE
				INC			EAX
				JMP			ONE_LOOP
	EVACUATE:				
				POP			EBX	
				POP			ECX	
				POP			EBP	
				RET			4			
	STRLEN			ENDP
		
	
	MAIN   			PROC
	
				MOV 			EDX, OFFSET MSG1
				CALL 			WRITESTRING

				MOV	    		ECX, SIZEOF BUFFER 
				MOV	   		EDX, OFFSET BUFFER
				CALL    		READSTRING

				PUSH			OFFSET BUFFER
				CALL			STRDCPY				
				
				MOV			EDX, OFFSET MSG2
				CALL 			WRITESTRING
				
				MOV			EDX, EAX
				CALL 			WRITESTRING
				
				FREE			EAX	
			
				EXIT
	MAIN 	    		ENDP

				END    			MAIN
