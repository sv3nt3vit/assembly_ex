TITLE draws box with horizontal lines 20x20	 (hw1030ILhorizontalBox.asm)

COMMENT @
Puprose: draw a box w/ horizontal lines in magenta
Programmer: Ihar Laziuk
Last updated: 11.04.2014
@

INCLUDE Irvine32.inc

.data

a dword 1
b dword 20		;FIXED VALUES
y byte  2
delta dword ?
blank byte " ",0
textcolor dword ?

.code

main PROC

	call clrscr	
	call create   
  	mov ecx, 20   

  	box:
    	push ecx
    	call drawLine
    	inc y
    	pop ecx
   	loop box
	
	call crlf
	
	mov textcolor, 15
    mov eax,(textcolor+(black*15))
    call settextcolor
			
exit

main ENDP
	
create proc
  	call clrscr
  	mov eax, b              
  	sub eax,a
  	inc eax
  	mov delta, eax       
  	ret	
create endp

drawLine proc

	mov edx, a 	    
	mov dh, y 	     
  	mov ecx, delta  
  	
	line:
		mov textcolor, 5
		mov eax, (textcolor + (black*15))
		call setTextColor	
		mov eax,'*'
		call gotoxy
		call writechar
		mov eax, 25
		call delay
		inc dl      
	loop line
    ret
drawLine endp
	
END main