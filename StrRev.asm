	TITLE STRREV

	; AUTHOR:	IHAR LAZIUK
	; DATE:		04.09.2015
	; CREATE A PROC THAT
	; REVERSES THE STRING
	; IMPLEMENT SWAP MACRO


	INCLUDE IRVINE32.INC

	SWAP		MACRO		OFFSET_OF_BUF, SIZE_OF_BUF	
				PUSH		EBP
				MOV 		EBP, ESP
				MOV 		BL,	OFFSET_OF_BUF
				MOV 		BH,	SIZE_OF_BUF
				
				XOR 		BL,	BH
				XOR 		BH,	BL
				XOR 		BL,	BH
				
				MOV 		OFFSET_OF_BUF, BL
				MOV 		SIZE_OF_BUF, BH
				POP			EBX			
				ENDM

	.DATA
				BUFFER 		BYTE 	20 DUP(0)	
				MSG1 		BYTE 	'ENTER A STRING ', 0
				MSG2 		BYTE 	'REVERSED STRING: ', 0
	.CODE


	STRREV 		PROC
				PUSH    	EBP
				MOV     	EBP, ESP
				PUSH    	EBX
				PUSH    	EDX
				PUSH		ECX				
				MOV     	EDX, [EBP+8]
				MOV 		ECX, [EBP+8]
				PUSH		[EBP+8]
				CALL 		STRLEN 
				ADD			ECX, EAX
				DEC			ECX
	
	ONE_LOOP:
				CMP			EDX, ECX
				JAE			EVACUATE		
				SWAP		[EDX],[ECX]
				DEC			ECX
				INC			EDX
				JMP			ONE_LOOP
	EVACUATE:
				POP			ECX
				POP	    	EDX
				POP     	EBX
				POP     	EBP
				RET     	4				
	STRREV    	ENDP

	STRLEN		PROC
						
				PUSH		EBP	
				MOV			EBP, ESP	
				PUSH		ECX				
				PUSH		EBX	
				MOV			ECX, [EBP + 8]

	ONE_LOOP:	MOV			BH, [ECX + EAX]
				CMP			BH, 0
				JE			EVACUATE
				INC			EAX
				JMP			ONE_LOOP
	EVACUATE:				
				POP			EBX	
				POP			ECX	
				POP			EBP	
				RET			4			
	STRLEN		ENDP

	MAIN	   	PROC	
                MOV 		EDX, OFFSET MSG1
				CALL 		WRITESTRING
				CALL 		CRLF
				
				MOV			ECX, SIZEOF BUFFER
				MOV			EDX, OFFSET BUFFER
				CALL		READSTRING
				
				PUSH		OFFSET BUFFER				
				CALL		STRREV 
				
				MOV 		EDX, OFFSET MSG2
				CALL 		WRITESTRING
				MOV			EDX, OFFSET BUFFER
				CALL 		WRITESTRING
				
				EXIT
	MAIN 		ENDP

				END			MAIN
