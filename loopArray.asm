TITLE loop array (loopArray.asm)

;Last update: 30.09.2014 written by Ihar Laziuk

INCLUDE Irvine32.inc

.code	;define vars

x byte 2
y byte 2
z dword 2
array1 byte 10,20,30,40
array2 word 100,200,300,10,20
a1 byte "array 1 byte ",0
space byte ' ',0


main PROC

	mov eax,0
	sub edx, offset a1
	call writestring
	mov ecx, lengthof array1
	mov esi,0
	
	array1loop:
		mov al, array1[esi]
		call writedec
		mov edx, offset space
		call writestring
		inc esi
	loop array1loop;
	
exit
main ENDP
END main
