TITLE   hw3ILarray         (hw3ILarray.asm)
; Last update: 09/29/2014   written by Ihar Laziuk

INCLUDE Irvine32.inc

.code
	w dword 12344567h, 231657ABh, 3004FFFEh

main  proc

mov eax,0
	mov eax,  [w]    
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [w+1]    
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [ w+2]    
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [w+3]    
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [ w+4]  
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [ w+5]  
	call writedec
	call crlf
	call writehex
	call crlf
	mov eax,  [ w+6]  
	call writedec
	call crlf
	call writehex
	call crlf
	
	exit
main ENDP
END main