TITLE Euclid's algorithm (THBIL6.asm)

COMMENT @
Puprose: demonstrates Euclid's algorithm
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@

INCLUDE Irvine32.inc

.data

userMsg1 byte 'Enter the 1st number : ', 0
userMsg2 byte 'Enter the 2nd number : ',0
GCDmsg byte 'The greatest common divisor is ',0

remainder dword ?
aa dword ?	;var to hold initial A value
bb dword ?	;var to hold initial B value
count dword 0
result dword ?

.code
main PROC

    call clrscr
	call crlf
	mov edx, offset userMsg1
     call writestring
     call readint
	 mov aa, eax
     call crlf
     mov edx, offset userMsg2
     call writestring
     call readint
	 mov bb, eax
     call crlf

	call Euclid
	
	mov edx, offset GCDmsg
	call writestring
	call writedec 	;eax contains the gcd
	call crlf

exit
main ENDP


euclid PROC 

  mov ecx, aa
  mov eax, bb
@@:
  cmp ecx, eax
  jge noswap	;jump if greater or equals
  xchg eax, ecx
noswap:
  sub ecx, eax
  jnz @B	;jump if not zero
  
; return value already in eax
  RET
euclid ENDP

end MAIN