TITLE loop b to a (loopDaemon4.asm)

;Last update: 30.09.2014 written by Ihar Laziuk

INCLUDE Irvine32.inc

.data 
	space byte ' ',0
	start dword 13
	last dword 20
	delta dword ?

.code	;define vars

main PROC

	mov eax,last
	sub eax, start
	inc eax
	mov delta, eax

	mov ecx,delta
	
	derRitt:
		mov eax, last
		call writedec
		mov edx, offset space
		call writestring
		dec last
	loop derRitt;
	
	call crlf
exit
main ENDP
END main
