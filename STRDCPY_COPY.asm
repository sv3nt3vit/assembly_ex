	TITLE STRDCPY

	; AUTHOR:	IHAR LAZIUK
	; DATE:		05.09.2015
	; CREATE A PROC THAT
	; PERFORMS DEEP COPY
	; OF A STRING

	INCLUDE IRVINE32.INC
	
	HANDLE		TEXTEQU		<DWORD>
	
	GetProcessHeap	PROTO
	HeapAlloc		PROTO,	HHEAP: HANDLE, DWFLAGS:DWORD, DWBYTES:DWORD
	HeapFree		PROTO, 	HHEAP: HANDLE, DWFLAGS:DWORD, LPMEM:DWORD	
	

	FREE		MACRO		HANDLE	
				PUSH		HANDLE
				CALL		MEMFREE
				ENDM
	
	
	MALLOC		MACRO		ALLOCSIZE	
				PUSH		ALLOCSIZE
				CALL		MEMALLOC
				ENDM

	
	.DATA
				BUFFER 		BYTE 	30 DUP(0)
				COPIED 	    BYTE 	30 DUP(0)
				MSG1 		BYTE 	'ENTER A STRING ', 0
				MSG2 		BYTE 	'COPIED STRING: ', 0							
	
	.CODE
	
	
	MEMALLOC	PROC				
				PUSH 		EBP
				MOV			EBP, ESP
				;PUSH		EBX
				;MOV		EBX, [EBP+8]
				INC			EBX
				INVOKE 		GetProcessHeap
				INVOKE 		HeapAlloc, EAX, 8, [EBP+8]
				;POP		EBX
				POP 		EBP
				RET			4				
	MEMALLOC	ENDP
	
				
	MEMFREE		PROC			
				PUSH		EBP
				MOV			EBP, ESP
				;PUSH		EBX
				;MOV		EBX, [EBP+8]
				INVOKE      GetProcessHeap
				INVOKE		HeapFree, EAX, 0, [EBP+8]
				;POP		EBX
				POP			EBP
				RET			4				
	MEMFREE		ENDP
	
	
	STRLEN		PROC						
				PUSH		EBP	
				MOV			EBP, ESP	
				PUSH		ECX				
				PUSH		EBX	
				MOV			ECX, [EBP + 8]

	ONE_LOOP:	MOV			BH, [ECX + EAX]
				CMP			BH, 0
				JE			EVACUATE
				INC			EAX
				JMP			ONE_LOOP
	EVACUATE:				
				POP			EBX	
				POP			ECX	
				POP			EBP	
				RET			4			
	STRLEN		ENDP
	

	STRCPY 		PROC
				PUSH        EBP
				MOV			EBP, ESP	
				PUSH		ESI
				PUSH		EDI
				PUSH		EAX
				PUSH       	EBX
				
				MOV 		ESI, [EBP+12]
				MOV			EDI, [EBP+8]				
				PUSH		[EBP + 12 ]
				CALL       	STRLEN
				
				CMP			EAX, SIZEOF COPIED
				JLE			ONE_LOOP
				JG			EVACUATE
	ONE_LOOP:
				MOV			BL, [ESI]
				CMP			BL, 0
				JE			EVACUATE
				MOV         [EDI], BL
				INC			ESI
				INC			EDI
				JMP			ONE_LOOP	
	EVACUATE:	    
				POP			EBX
				POP			EAX
				POP			EDI
				POP			ESI
				POP 		EBP
				RET			8
	STRCPY	    ENDP
	
	
	STRDCPY		PROC				
				CALL		STRLEN
				MALLOC		EAX
				PUSH		EAX
				CALL		STRCPY
				RET			4
	STRDCPY		ENDP

	
	MAIN   		PROC
	
				MOV 		EDX, OFFSET MSG1
				CALL 		WRITESTRING
				CALL 		CRLF
				
				MOV	    	ECX, SIZEOF BUFFER 
				MOV	   		EDX, OFFSET BUFFER
				CALL    	READSTRING
				CALL    	CRLF
				
				PUSH    	OFFSET BUFFER
				CALL		STRDCPY			
				
				MOV 		EDX, OFFSET MSG2
				CALL 		WRITESTRING
				CALL 		CRLF
				
				PUSH    	OFFSET COPIED

				MOV 		EDX, OFFSET COPIED
				CALL 		WRITESTRING
				
				POP			EAX
				FREE		EAX
								
				EXIT
	MAIN 	    ENDP

				END    		MAIN
