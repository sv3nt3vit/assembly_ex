TITLE draws box with vertical lines 20x20	 (hw1030ILverticalBox.asm)

COMMENT @
Puprose: draw a box w/ vertical lines in magenta
Programmer: Ihar Laziuk
Last updated: 11.04.2014
@

INCLUDE Irvine32.inc

.data

a dword 1
b dword 20	
y byte  5
delta dword ?
blank byte " ",0
textcolor dword ?

.code

main PROC

	call clrscr	
	call created   
  	mov ecx, 20   

  	box:
    	push ecx
    	call drawLine
    	inc y
    	pop ecx
   	loop box
	
	call crlf
	
	mov textcolor, 15
    mov eax,(textcolor+(black*15))
    call settextcolor
				
exit

main ENDP
	
created proc

  	call clrscr
  	mov eax, b              
  	sub eax,a
  	inc eax
  	mov delta, eax       
  	ret
	
created endp

drawLine proc

	mov edx, a 	    
  	mov dl, y	;horizontally start at y
	mov dh, 2 	;vertically start at 5        
  	mov ecx, delta  
  
    line:
		mov textcolor, 5
		mov eax, (textcolor + (black*15))
		call settextcolor
     	mov eax,'*'
     	call gotoxy
     	call writechar
		mov eax, 25
		call delay
     	inc dh       
    loop line;

    ret

drawLine endp

END main