TITLE counts integers                             (hw1014ilIntCount.asm)

;This Program prints out multiplication table for numbers 1-10
;Last updated 10/16/2014   Written by Ihar Laziuk
INCLUDE Irvine32.inc

.data
	array byte 1,2,3,1,3,4,4,4,1,2,2,1,4,5
	;cnt byte 92
	 
.code
main PROC		
		call clrscr
		call crlf
		
		mov eax,0
		mov ecx,13
		mov esi,0
         
	L:		
		mov al, array+[esi]
		mov eax, esi
        call writedec
		;inc cnt[esi]
		inc esi
		call crlf
    loop L
        
        exit	 
main ENDP
end main