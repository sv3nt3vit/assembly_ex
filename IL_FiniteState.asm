TITLE FiniteState (THILfiniteState.asm)

COMMENT @
Puprose: demonstrates finite state
Programmer: Ihar Laziuk
Last updated: 12.06.2014
@

INCLUDE Irvine32.inc
.data

msg byte ' Enter the first input ',0
msg1 byte ' Enter ther second input ',0
state byte '	state is now ',0
output byte '	ouptut is now ',0


.code
main PROC
    call clrscr
	
	mov edx, offset state
	call writestring
	mov eax,0
	call writedec
	call crlf
	mov edx, offset output
	call writestring
	call writedec
	call crlf
	
    mov edx, offset msg
    call writestring
    call readint
    mov ebx, eax
    mov edx, offset msg1
    call writestring
    call readint
    call crlf

    .if(eax==0 && ebx==0)
	call s0

    .elseif(eax == 1 && ebx ==1)
	
	call s1
    .elseif(eax == 0 && ebx == 1)
        call s2

    .elseif(eax==1 && ebx ==0)
	call s3

    .endif


exit
main ENDP

s0 PROC

    mov edx, offset state
    call writestring
    mov eax, 0
    call writedec
    call crlf
    mov edx, offset output
    call writestring
    mov eax, 0
    call writedec
    call crlf
    mov edx, offset msg
    call writestring
    call readint
    mov ebx, eax
    mov edx, offset msg1
    call writestring
    call readint
    call crlf

    .if(eax==0 && ebx==0)
	call s0

    .elseif(eax==1 && ebx==1)
        call s1
    
     .elseif (eax==0 && ebx==1)
	call s2

      .elseif (eax==1 && ebx==0)
 	call s2
     .endif
ret
s0 ENDP

s1 PROC

    mov edx, offset state
    call writestring
    mov eax, 1
    call writedec
    call crlf
    mov edx, offset output
    call writestring
    mov eax, 0
    call writedec 
    call crlf
    mov edx, offset msg
    call writestring
    call readint
    mov ebx, eax
    mov edx, offset msg1
    call writestring
    call readint
    call crlf

    .if(eax==0 && ebx==0)
	call s2

    .elseif(eax==1 && ebx==1)
        call s3
    
     .elseif (eax==0 && ebx==1)
	call s2

      .elseif (eax==1 && ebx==0)
 	call s2
     .endif


ret
s1 ENDP

s2 PROC

    mov edx, offset state
    call writestring
    mov eax, 2
    call writedec
    call crlf
    mov edx, offset output
    call writestring
    mov eax, 1
    call writedec 
    call crlf
    mov edx, offset msg
    call writestring
    call readint
    mov ebx, eax
    mov edx, offset msg1
    call writestring
    call readint
    call crlf

    .if(eax==0 && ebx==0)
	call s0

    .elseif(eax==1 && ebx==1)
        call s1
    
     .elseif (eax==0 && ebx==1)
	call s2

      .elseif (eax==1 && ebx==0)
 	call s2
     .endif


ret
s2 ENDP


s3 PROC

    mov edx, offset state
    call writestring
    mov eax, 3
    call writedec
    call crlf
    mov edx, offset output
    call writestring
    mov eax, 1
    call writedec 
    call crlf
    mov edx, offset msg
    call writestring
    call readint
    mov ebx, eax
    mov edx, offset msg1
    call writestring
    call readint
    call crlf

    .if(eax==0 && ebx==0)
	call s2

    .elseif(eax==1 && ebx==1)
        call s2
    
     .elseif (eax==0 && ebx==1)
	call s1

      .elseif (eax==1 && ebx==0)
 	call s1
     .endif


ret
s3 ENDP

END main

