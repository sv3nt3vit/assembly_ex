TITLE read theLength & print line	 (hw1030ILreadLength.asm)

COMMENT @
Puprose: read user's input for the
length of the string and print it
Programmer: Ihar Laziuk
Last updated: 11.04.2014
@

INCLUDE Irvine32.inc

.data

a dword ?	
y byte  1
delta dword ?
blank byte " ",0
ask byte "Enter desired length of string ",0  
textcolor dword ?

.code

main PROC

	call create  
	call drawline
	call crlf	
	mov textcolor, 15
    mov eax,(textcolor+(black*15))
    call settextcolor
			
exit

main ENDP
	
create proc
  	call clrscr
	mov edx, offset ask
	call writestring	;ask for the length of string
	call readint
	mov a, eax 		;start
  	mov delta, eax       
  	ret	
create endp

drawLine proc

	mov edx, a 	    
	mov dh, y 	     
  	mov ecx, delta  
  	
	line:
		mov textcolor, 5
		mov eax, (textcolor + (black*15))
		call setTextColor	
		mov eax,'>'
		call gotoxy
		call writechar
		mov eax, 25
		call delay
		inc dl      
	loop line
    ret
drawLine endp
	
END main