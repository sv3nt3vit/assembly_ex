TITLE   hw3ILexchange         (hw3ILexchange.asm)
; Last update: 09/29/2014   written by Ihar Laziuk

INCLUDE Irvine32.inc

.code
	x byte 1
	y byte 2
	temp byte 0
	
main  proc
	
	mov ah,x
	mov temp,ah
	mov y,ah
	mov x, temp
	
	call DumpRegs
	call writedec
	
	exit
main  endp
end   main
