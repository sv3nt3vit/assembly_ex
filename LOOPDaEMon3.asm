TITLE loop a to b (loopDaemon3.asm)

;Last update: 30.09.2014 written by Ihar Laziuk

INCLUDE Irvine32.inc

.data 
	space byte ' ',0
	start dword -5
	last dword 32
	delta dword ?

.code	;define vars

main PROC

	mov eax,last
	sub eax, start
	inc eax
	mov delta, eax

	mov ecx,delta
	
	derRitt:
		mov eax, start
		call writeint
		mov edx, offset space
		call writestring
		inc start
	loop derRitt;
	
	call crlf
exit
main ENDP
END main
