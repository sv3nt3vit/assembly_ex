TITLE Alphabet (THBIL4.asm)

COMMENT @
Puprose: prints the alphabet
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@

INCLUDE Irvine32.inc

.data
	blank byte ' ',0
	begin dword 41h	;41h = A, alphabet starts in ASCII
	end_ dword 5Ah	;5Ah = Z, alphabet ends in ASCII
	delta dword ?

.code

main PROC

	call clrscr
	call crlf
	
	call for_

	call crlf 
	exit
	
main ENDP

for_ PROC
	
	mov eax,0
	mov eax, end_
	sub eax, begin
	inc eax
	mov delta, eax
	
	mov eax, 0
	
	.while eax < delta
	
		inc eax
		push eax
		mov eax, begin
		call writechar
		mov edx, offset blank
		call writestring 
		inc begin
		pop eax
		
	.endw

ret
for_ ENDP

END main

