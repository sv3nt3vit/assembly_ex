TITLE printing initials         (THBIL1a-b-c.asm)
  
COMMENT @
Puprose: a) print my initials
		 b) shift initials to the left
		 c) print initials upside down
Programmer: Ihar Laziuk
Last updated: 11.06.2014
@

INCLUDE Irvine32.inc

.data

line0 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
line1 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
line2 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
line3 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
line4 byte 1,' ', 2,'x',3,' ',5,'x',1,' ' 
line5 byte 1, " " 

l0 = lengthof line0
l1 = lengthof line1
l2 = lengthof line2
l3 = lengthof line3
l4 = lengthof line4
l5 = lengthof line5


delta dword ?
l dword ?

grandARRAY dword 12 dup(?)

x byte 40
Y byte 1

.code

main PROC

	call clrscr
	call fillall
	mov ecx, 30

movement2:
	
	push ecx
	
	mov dh, y
	mov dl, x
	call gotoxy

	call letters
	mov eax, 300
	call delay	
	dec x
	
	pop ecx
	
loop movement2


;------------------------------
	call fillall2
	
	mov eax, 400
	call delay
	
	call clrscr
	
	mov dh, y
	mov dl, x
	call gotoxy
	
	call letters
;-------------------------------


exit

printline PROC

	line:

		call writechar
		inc dl

	loop line;
ret

printline endp

fillall PROC

  mov esi,	offset grandarray
  
  mov [esi],	offset line0
  add esi,4
  mov ebx, 	l0/2
  mov [esi],	ebx
  add esi,4
  mov [esi],	offset line1
  add esi,4
  mov ebx, 	l1/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset line2
  add esi,4
  mov ebx, 	l2/2
  mov [esi],	ebx
  add esi,4
  mov [esi],   offset line3
  add esi,4
  mov ebx, 	l3/2
  mov [esi],	ebx
  add esi, 4
  mov [esi],	offset line4
  add esi,4
  mov ebx, 	l4/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset line5
  add esi,4
  mov ebx, 	l5/2
  mov [esi],	ebx

ret

fillall endp

;-------------------------------
fillall2 PROC

  mov esi,	offset grandarray
  mov [esi],	offset line4
  add esi,4
  mov ebx, 	l0/2
  mov [esi],	ebx
  add esi,4
  mov [esi],	offset line3
  add esi,4
  mov ebx, 	l1/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset line2
  add esi,4
  mov ebx, 	l2/2
  mov [esi],	ebx
  add esi,4
  mov [esi],   offset line1
  add esi,4
  mov ebx, 	l3/2
  mov [esi],	ebx
  add esi, 4
  mov [esi],	offset line0
  add esi,4
  mov ebx, 	l4/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset line5
  add esi,4
  mov ebx, 	l5/2
  mov [esi],	ebx

ret

fillall2 endp
;-----------------------------

letters proc

	mov esi, offset grandarray
	sub esi, 4
	mov ecx, 6

	outer:
		push ecx

		mov eax, 0

		add esi, 4
		mov edi, [esi]
		add esi, 4
		mov ecx, [esi]
		
		inner:
			
			push ecx
			mov ecx, 0		
			mov cl, [edi]
			inc edi
			mov al, [edi]
			call printline

			inc edi
			pop ecx
			
		loop inner;
		
		inc dh
		mov dl, x
		call gotoxy
		pop ecx
		
	loop outer;	

ret

letters endp

main ENDP
END main
 