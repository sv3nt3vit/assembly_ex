	TITLE Project_1

	; Author:	Ihar Laziuk
	; Date:		02.5.2015

	INCLUDE IRVINE32.INC

	.DATA

						MSG1 		BYTE 	'ENTER YOUR NAME ', 0
						MSG2 		BYTE 	'YOUR NAME IS ', 0
						UNAME 		BYTE 	21 DUP(0)	

	.CODE
	
	MAIN 				PROC

						MOV 		EDX, 	OFFSET MSG1
						CALL 		WRITESTRING
						CALL 		CRLF
							 
						MOV 		ECX, 	SIZEOF UNAME			; SPECIFY MAX # OF CHARACTERS
						MOV 		EDX, 	OFFSET UNAME			; POINT TO UNAME
							 
						CALL 		READSTRING						; INUPT THE STRING
						MOV 		EDX, 	OFFSET MSG2
						CALL 		WRITESTRING
						MOV 		EDX, 	OFFSET UNAME
						CALL 		WRITESTRING
		
						EXIT
	MAIN 				ENDP
	
						END 		MAIN
