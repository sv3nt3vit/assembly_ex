TITLE  draw a diamond     (hw1030ildiamond.asm)

;Description: program draws a diamond and reverse
; Programmer: Ihar Laziuk
; Last updated: 10.30.2014

Include Irvine32.inc
 
.data			 
	line1 dword ?
	line2 dword ?
	empty dword ?
	stars dword ?
	symbol byte ?
          
.code
	main PROC
	   call clrscr	
           call initialcond
           call wholed
           call initialcond
           call wholed 
           	   
exit
main ENDP



        drawdiamond proc
            mov ecx, line1
            linesloop:
               mov ecx, empty
               mov symbol, " "
               call drawline

           ; prints the 7 blanks




	       mov ecx, stars
               mov symbol, "*"
               call drawline
               call crlf
               dec empty
               mov eax,stars
               add eax,2
               mov stars,eax
               dec line1
               mov ecx, line1
                
            loop linesloop
            
	    ret

         drawdiamond endp
         drawdiamonddown proc
            mov ecx, line1
           linesloop:
               mov ecx, empty
               mov symbol, " "
               call drawline
	       mov ecx, stars
               mov symbol, "*"
               call drawline
               call crlf
               inc empty
               mov eax,stars
               sub eax,2
               mov stars,eax
               dec line1
               mov ecx, line1
                
            loop linesloop
            
	    ret

         drawdiamonddown endp
	 drawline proc
              lineloop:
		  mov al,symbol
		  call writechar
	      loop lineloop  
         ret

         drawline endp
         wholed proc
            call drawdiamond   ;draws the top part
            ; the following draws the bottom and initialzes the stars and blank
            mov eax,line2  ; the 7
            mov line1,eax      ; print 6 character 
            
           
           inc empty
           dec stars
           dec stars
           
	   call drawdiamonddown
         ret
         wholed endp

         initialcond proc
           mov eax,7   ; number of lines
           mov line1, eax    ; 7 lines for the triangle
           mov line2,eax    ;save for ;later use
           
	   dec eax
	   mov empty,eax   ; 7 blanks to start
             
	   mov eax, 1 
	   mov stars, eax  ; initial stars  = 1
         ret
         initialcond endp
END main


call crlf  - next line 