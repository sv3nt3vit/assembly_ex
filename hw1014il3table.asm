TITLE 3table   special edition                            (hw1014il3table.asm)
 
;This Program prints out multiplication table for 3
;Last updated 10/16/2014   Written by Ihar Laziuk
INCLUDE Irvine32.inc
.data
	i byte 0
	timez byte "*"
	equals byte "="
	sum dword ?
	number dword 3
	 
.code
main PROC		
		call clrscr
		call crlf
		
		mov eax,0
		mov ecx,11
         
	Execute:
		mov eax, number
		call writedec
		
		mov eax,0
		mov al, timez
		call writechar
		
		mov al,i
		call WriteDec
		
		mov eax,0
		mov al, equals
		call WriteChar
		mov eax,0
		mov eax, sum
		call WriteDec
		
		call crlf
		mov eax, number
		add sum, eax
		inc i
    loop Execute
        
        exit	 
main ENDP
end main