TITLE Division: remainder and quotient (THBIL5.asm)

COMMENT @
Puprose: performs division
		 by subtraction, 
		 displays count and
		 the remainder, error
		 if division by 0.
		 Implemented in a proc.
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@

INCLUDE Irvine32.inc

.data

userMsg1 byte 'Enter number A, such that A > 0 : ', 0
userMsg2 byte 'Enter number B, such that B > A : ',0
countMsg byte 'The count is ',0
remainderMsg byte 'Remainder is ',0
errorMsg byte 'ERROR: division by zero undefined',0
divOp byte ' / ',0
equalsOp byte ' = ',0
plusOp byte ' + R ',0
remainder dword ?
a dword ?
aa dword ?	;var to hold initial A value
bb dword ?	;var to hold initial B value
count dword 0
result dword ?

.code
main PROC

    call clrscr
	call crlf
	;mov edx, offset userMsg1
    ; call writestring
    ; call readint
    mov eax,10h
     mov a, eax
	 mov aa, eax	;just to print later
     mov result, eax
     call crlf
   ;  mov edx, offset userMsg2
    ; call writestring
    ; call readint
      mov eax,54h
     
	 mov bb, eax	;just to print later
     call crlf
	call division
	call crlf

exit
main ENDP


division PROC

.while(eax>=a && eax>0)

	.if (a==0)
		mov edx, offset errorMsg
		call writestring
		call crlf
		jmp evacuate
	.endif

    sub eax, a
    inc count
	  		
    .if(eax<a)
		push eax
		mov remainder, eax
		pop eax
    .elseif(eax==a)
		mov remainder, 0
    .endif
.endw
	  
.if(eax<a)
		mov remainder, eax
.endif

	   mov eax, bb
       call writehex
	   mov edx, offset divOp
	   call writestring
	   mov eax, aa
	   call writehex
	   mov edx, offset equalsOp
	   call writestring
       mov eax, count
       call writehex
	   mov edx, offset plusOp
	   call writestring
       mov eax, remainder
       call writehex
	evacuate:

ret
division ENDP

end MAIN