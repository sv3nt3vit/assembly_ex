TITLE   hw3ILaddNum         (hw3ILaddNum.asm)
; Last update: 09/29/2014   written by Ihar Laziuk

INCLUDE Irvine32.inc

.code
	x byte 1
	y byte 2
	z byte 

main  proc

	mov eax, x
	mov ebx, y
	add eax, ebx
	call writedec
	call crlf	

main  endp
end   main
