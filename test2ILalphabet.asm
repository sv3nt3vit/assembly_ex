TITLE Alphabet (test2ILalphabet.asm)

COMMENT @
Puprose: prints the alphabet
Programmer: Ihar Laziuk
Last updated: 11.29.2014
@

INCLUDE Irvine32.inc

.data
	blank byte ' ',0
	begin dword 41h
	end_ dword 5Ah

.code

main PROC

mov ecx, 26
mov eax, 0
;sub ecx, begin
;inc ecx
;mov eax, ecx

	ABC:
		
		mov eax, begin
		call writechar
		mov edx, offset blank
		call writestring 
		inc begin

	loop ABC

	call crlf 
	exit
main ENDP
END main

