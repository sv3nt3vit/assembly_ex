TITLE printing initials  upside down        (initialsUpDown.asm)
  
COMMENT @
Puprose: print my initials upside down
Programmer: Ihar Laziuk
Last updated: 11.06.2014
@

INCLUDE Irvine32.inc

.data

liner1 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
liner2 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
liner3 byte 1,' ', 2,'x',3,' ',2,'x',1,' ' 
liner4 byte 1,' ', 2,'x',3,' ',5,'x',1,' ' 
liner5 byte 1, " " 

l1 = lengthof liner1
l2 = lengthof liner2
l3 = lengthof liner3
l4 = lengthof liner4
l5 = lengthof liner5

delta dword ?
l dword ?

grandARRAY dword 10 dup(?)

x byte 40
Y byte 1

.code

main PROC

	call clrscr
	
	mov ecx,30
	push ecx
 	
	call fillall
	
	movement:
	;mov eax, 300
	;call delay
	call letters 
 	dec x  
	call gotoxy
	pop ecx
loop movement
	
	;mov dh, y
	;mov dl, x
	;call gotoxy
	
	;call letters
	
		
	call fillall2
	
	mov eax, 400
	call delay
	
	call clrscr
	
	mov dh, y
	mov dl, x
	call gotoxy
	
	call letters
	
exit

printline PROC

	line:

		call writechar
		inc dl

	loop line;
ret

printline endp


fillall PROC

  mov esi,	offset grandarray
  mov [esi],	offset liner1
  add esi,4
  mov ebx, 	l1/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset liner2
  add esi,4
  mov ebx, 	l2/2
  mov [esi],	ebx
  add esi,4
  mov [esi],   offset liner3
  add esi,4
  mov ebx, 	l3/2
  mov [esi],	ebx
  add esi, 4
  mov [esi],	offset liner4
  add esi,4
  mov ebx, 	l4/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset liner5
  add esi,4
  mov ebx, 	l5/2
  mov [esi],	ebx

ret
fillall endp


fillall2 PROC

  mov esi,	offset grandarray
  mov [esi],	offset liner4
  add esi,4
  mov ebx, 	l1/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset liner3
  add esi,4
  mov ebx, 	l2/2
  mov [esi],	ebx
  add esi,4
  mov [esi],   offset liner2
  add esi,4
  mov ebx, 	l3/2
  mov [esi],	ebx
  add esi, 4
  mov [esi],	offset liner1
  add esi,4
  mov ebx, 	l4/2
  mov [esi],	ebx
  add esi,4
  mov [esi],  offset liner5
  add esi,4
  mov ebx, 	l5/2
  mov [esi],	ebx

ret

fillall2 endp



letters proc

	mov esi, offset grandarray
	sub esi, 4
	mov ecx, 5

	inner1:
		push ecx

		mov eax, 0

		add esi, 4
		mov edi, [esi]
		add esi, 4
		mov ecx, [esi]
		
		inner2:
			
			push ecx
			mov ecx, 0		
			mov cl, [edi]
			inc edi
			mov al, [edi]
			call printline

			inc edi
			pop ecx
			
		loop inner2;
		
		inc dh
		mov dl, x
		call gotoxy
		pop ecx
		
	loop inner1;	

ret

letters endp


main ENDP
END main
 