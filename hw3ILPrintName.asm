TITLE   hw3ILPrintName         (hw3ILPrintName.asm)
; Last update: 09/29/2014   written by Ihar Laziuk

INCLUDE Irvine32.inc

.code
	print_name byte "Ihar Laziuk",0
	nameSize = ($-print_name)

main  proc

	mov edx, offset print_name
	call writestring
	call crlf
	
	mov eax, nameSize
	call writedec
	call crlf
	
	mov edx, TYPE print_name
	call writestring
	;call crlf
	
Exit	
main  endp
end   main
